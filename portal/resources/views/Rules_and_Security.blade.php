@extends('template')

@section('content')
    <div class="text-gray-400 pr-5 pl-5 pt-3 pb-3 bg-black">

        <span class="text-primary font-weight-bold d-block pt-4">1️⃣ Be respectful & civil – language is flexible to a certain extent</span>
        <span class="d-block">1.1 Don’t be rude and disrespectful</span>
        <span class="d-block">1.1.1 Especially respect all moderator & admin ranks</span>
        <span class="text-primary font-weight-bold d-block pt-4">1.2 Do not use alternate accounts to evade mutes and bans in order to continue harassing or bullying other players, nor writing generally inappropriate comments</span>
        <span class="d-block">1.2.1 Do not use your friends to appeal mutes or bans either. Friends who appeal on behalf of others may be muted/banned themselves</span>
        <span class="text-primary font-weight-bold d-block pt-4">1.3 No NSFW & NSFL content/discussions/player names</span>
        <span class="text-primary font-weight-bold d-block pt-4">1.4 Do not flame or attack others</span>
        <span class="d-block">1.4.1 No hate speech or controversial topics such as Politics, Religion, Race, anti-LGBTQIA+ etc.</span>
        <span class="d-block">1.4.2 No discrimination</span>
        <span class="text-primary font-weight-bold d-block pt-4">1.5 No impersonating staff members</span>
        <span class="d-block">1.6 Do not abuse tagging/mentions</span>
        <span class="d-block">1.7 Do not encourage rule breaking</span>
        <span class="d-block">1.8 No scamming & no gambling</span>
        <span class="d-block">1.9 No offensive account names</span>

        <span class="text-primary font-weight-bold d-block pt-4">2️⃣ Do not use bots, autos & macros in our games, the only exception being in RSC Uranium and RSC Coleslaw</span>

        <span class="text-primary font-weight-bold d-block pt-4">3️⃣ Do not exploit vulnerabilities or bugs. Responsibly disclose them so they can be fixed. Bug abuse will result in permanent banning from all worlds, Discord, and other platforms</span>
        <span class="d-block">3.1 In 2001scape, it is not allowed to create alt accounts to exploit quest rewards. The use of mules is allowed</span>

        <span class="text-primary font-weight-bold d-block pt-4">4️⃣ No Real World Trading (RWT). Anything outside the OpenRSC game(s) (even from Jagex) is considered RWT</span>
        <span class="d-block">4.1 No trading between botting (Uranium & Coleslaw) and non-botting (Preservation, Cabbage & 2001scape) worlds</span>
        <span class="d-block">4.2 Account buying and selling is not allowed.</span>
        <span class="d-block">4.2.1 Account sharing is not supported or allowed. This is due to staff being unable to separate identities if one person breaks the rules and it is unfair to those competing for hiscores.</span>

        <span class="text-primary font-weight-bold d-block pt-4">5️⃣ No illegal activity – Follow all Government Laws & all involved (Discord etc.) Terms of Services</span>
        <span class="d-block">5.1 Don’t tell people where to buy mind-altering substances, AKA drugs. Don’t endorse drugs unless they are also found in RSC (i.e. Whiskey is okay)</span>

        <span class="text-primary font-weight-bold d-block pt-4">6️⃣ No Advertising</span>
        <span class="d-block">6.1 Discussion of unapproved Discord Servers or posting invite links (without permission) is forbidden</span>
        <span class="d-block">6.2 Discussion of closed source or for-profit RSC-based private game servers or websites is forbidden</span>
        <span class="d-block">6.3 Any form of unwanted DM/PM advertising is forbidden</span>
        <span class="d-block">6.4 Do not advertise Open RSC to others as part of unsolicited DM/PMs, or in other servers where permission has not been granted. This is not helpful to us. It is a bad look for the project to advertise Open RSC in spaces which are dedicated to other RS communities.</span>

        <span class="text-primary font-weight-bold d-block pt-4">7️⃣ Use English in public chats. Brief snippets of other languages are OK if it is relevant to discussion of those languages.</span>
        <span class="d-block">7.1 You must only use English in Global chat in game.</span>

        <span class="text-primary font-weight-bold d-block pt-4">8️⃣ There is a limit to how many accounts you can play simultaneously, but not a limit on how many you may register & play on in general. Exceeding this limit may result in a temporary IP ban. The account limits are subject to change from time to time, you can always check the latest limit by typing ::mppi in-game</span>
        <span class="d-block">8.1 You are allowed to play 3 accounts simultaneously on RSC Preservation.</span>
        <span class="d-block">8.2 You are allowed to play 3 accounts simultaneously on RSC Cabbage.</span>
        <span class="d-block">8.3 You are allowed to play 2 accounts simultaneously on 2001scape.</span>
        <span class="d-block">8.4 You are allowed to play 10 accounts simultaneously on RSC Uranium.</span>
        <span class="d-block">8.5 You are allowed to play 20 accounts simultaneously on RSC Coleslaw.</span>

        <span class="font-weight-bold d-block pt-4">*️⃣ We reserve the right to remove misbehaving players at our discretion even if they haven't explicitly broken any of the above rules.</span>
        <span class="d-block">*️⃣ These rules are enforced at our discretion. We typically reach out to people before banning when they have behavioral problems or trouble getting along with others, but in some cases, people who don't seem like they will change their behaviour may be banned without the courtesy of a warning.</span>
        <span class="d-block">*️⃣ Our moderation team are unpaid volunteers who have a limited capacity to withstand abuse. Insulting staff is a sure way to be removed from almost any community.</span>
        <span class="d-block">*️⃣ We run this as a free service, open source so anyone can run a copy themselves. We also offer account exports, so that even if you are banned, you can take your account somewhere else.</span>

    </div>
    <script>
        window.onload = function() {
            localStorage.setItem('visitedRules', 'true');
        };
    </script>
@endsection
