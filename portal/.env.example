APP_NAME="OpenRSC"
APP_ENV="local" # local or production
APP_KEY=
APP_DEBUG=true
APP_URL=http://rsc.vet

APP_TIMEZONE=UTC
APP_LOCALE=en
APP_FALLBACK_LOCALE=en
APP_FAKER_LOCALE=en_US
APP_MAINTENANCE_DRIVER=file
APP_MAINTENANCE_STORE=database
BCRYPT_ROUNDS=10
TIMEZONE=America/New_York

RECAPTCHA_SITE_KEY=null
RECAPTCHA_SECRET_KEY=null

DB_CONNECTION=mysql
DB_HOST=mariadb
DB_PORT=3306
#LARAVEL_DATABASE=
DB_USERNAME=root
DB_PASSWORD=root
LOG_SQL=false

MAIL_MAILER=smtp
MAIL_HOST=mailhog
MAIL_PORT=1025
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=null
MAIL_FROM_NAME="${APP_NAME}"

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"

LOG_CHANNEL=stack
LOG_STACK=single
LOG_LEVEL=debug

SESSION_ENCRYPT=false
SESSION_PATH=/
SESSION_DOMAIN=null

#Optional toggles
STATS_PAGE_GENERATES_CSV="false" # Do we want to generate a CSV when viewing the stats page?
STATS_HOURLY_JOB_ENABLED="false" # Do we want to automatically generate stats hourly for each world?
STATS_HOURLY_CSV_JOB_ENABLED="false" # Do we want to automatically generate a stats CSV hourly for each world? If STATS_HOURLY_CSV_JOB_ENABLED is true, STATS_HOURLY_JOB_ENABLED is ignored.
STATS_DAILY_JOB_ENABLED="false" # Do we want to automatically generate stats daily for each world?
STATS_DAILY_CSV_JOB_ENABLED="false" # Do we want to automatically generate a stats CSV daily for each world? If this is true, STATS_DAILY_JOB_ENABLED is ignored.
STATS_WEEKLY_JOB_ENABLED="false" # Do we want to automatically generate stats weekly for each world?
STATS_WEEKLY_CSV_JOB_ENABLED="false" # Do we want to automatically generate a stats CSV weekly for each world? If this is true, STATS_WEEKLY_JOB_ENABLED is ignored.
STATS_PAGE_ENABLED="true" # Do we want to enable the stats page?
FORCE_HTTPS="false" # Do we want to force HTTPS for all pages?
GPG_PUBLIC_KEY_FILE="openrsc-gpg-public-key.key" #Location to the GPG public key file
GPG_PRIVATE_KEY_FILE="" #Location to the GPG private key file
PLAYER_EXPORTS_ENABLED="true" #Do we want player exports?
PLAYER_EXPORTS_API_ENABLED="true" #Do we want player exports API access?
PLAYER_EXPORTS_ADMIN_ONLY="false" #Do we want to require admin authentication to run player exports?
PLAYER_EXPORTS_MODERATOR_ONLY="false" #Do we want to require moderator authentication to run player exports?
LOGIN_ENABLED="true" #Do we want to enable login?
LOGIN_ADMIN_ONLY="false" #Do we want to enable admin-only login?
LOGIN_LOGGING_ENABLED="true" #Do we want to enable logging logins?
BOARD_ENABLED="false" #Do we want to enable forum integration?
WEB_REGISTRATION_ENABLED="true" #Do we want to enable web registration?
API_REGISTRATION_ENABLED="true" #Do we want to enable API registration?
MAX_NEW_ACCOUNTS_PER_24_HOURS_PRESERVATION="3" #Maximum number of new accounts per IP per 24 hours per Preservation DB
MAX_NEW_ACCOUNTS_PER_24_HOURS_CABBAGE="3" #Maximum number of new accounts per IP per 24 hours per Cabbage DB
MAX_NEW_ACCOUNTS_PER_24_HOURS_2001SCAPE="2" #Maximum number of new accounts per IP per 24 hours per 2001Scape DB
MAX_NEW_ACCOUNTS_PER_24_HOURS_OPENPK="3" #Maximum number of new accounts per IP per 24 hours per OpenPK DB
MAX_NEW_ACCOUNTS_PER_24_HOURS_URANIUM="10" #Maximum number of new accounts per IP per 24 hours per Uranium DB
MAX_NEW_ACCOUNTS_PER_24_HOURS_COLESLAW="20" #Maximum number of new accounts per IP per 24 hours per Coleslaw DB
NPC_HISCORES_ENABLED="true" #Do we want to enable NPC hiscores?
NPC_OVERALL_HISCORES_ENABLED="true" #Do we want to enable NPC overall hiscores?
NPC_ODYSSEY_HISCORES_ENABLED="true" #Do we want to enable NPC Odyssey hiscores?
DISCORD_URL="https://discord.gg/ABdFCqn" #The Discord url we want to use in the navbar and optionally the maintenance page
DISCORD_URL_ON_MAINTENANCE_PAGE="true" #Do we want the Discord url to display on the maintenance page?
CACHING_DATABASES="false" #Do we want to use the caching databases in place of realtime ones wherever possible?
MULTI_WORLD_LOGINS="true" #Do we want to be able to log into accounts for more worlds than just preservation?
MESSAGE_CENTER_ENABLED="true" #Do we want users to be able to see the message center?
MESSAGE_CENTER_APPEAL_MESSAGE_ENABLED="true" #Do we want users to be able to see the message center appeal message?
